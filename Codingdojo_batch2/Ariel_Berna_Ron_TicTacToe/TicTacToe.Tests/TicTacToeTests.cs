using TicTacToe.App.Services;

namespace TicTacToe.Tests;

public class TicTacToeTests
{
    private static TicTacToeService CreateTicTacToeService()
    {
        return new TicTacToeService();
    }

    private static void SetPlayerMoves(
        TicTacToeService sut,
        int[] moveIndexes,
        char moveValue)
    {
        foreach (var moveIndex in moveIndexes)
        {
            sut.PlayerMove(moveIndex, moveValue);
        }
    }

    public class PlayerMove
    {
        [Theory]
        [InlineData('Z')]
        [InlineData('A')]
        [InlineData('B')]
        [InlineData('C')]
        public void GivenInvalidMoveValue_ThrowsArgumentException(char moveValue)
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            var actual = Assert.Throws<ArgumentException>(() => sut.PlayerMove(0, moveValue));

            // Assert
            Assert.True(actual != null);
            Assert.Equal("Invalid move value.", actual.Message);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(-2)]
        [InlineData(10)]
        [InlineData(15)]
        public void GivenInvalidMoveIndex_ThrowsArgumentException(int index)
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            var actual = Assert.Throws<ArgumentException>(() => sut.PlayerMove(index, 'X'));

            // Assert
            Assert.True(actual != null);
            Assert.Equal("Invalid move index.", actual.Message);
        }
    }

    public class GetWinner
    {
        [Theory]
        [InlineData(new int[] {0,1,2}, 'O', 1)]
        [InlineData(new int[] {0,1,2}, 'X', 2)]
        public void GivenFirstRowHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }

        [Theory]
        [InlineData(new int[] {3,4,5}, 'O', 1)]
        [InlineData(new int[] {3,4,5}, 'X', 2)]
        public void GivenSecondRowHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }

        [Theory]
        [InlineData(new int[] {6,7,8}, 'O', 1)]
        [InlineData(new int[] {6,7,8}, 'X', 2)]
        public void GivenThirdRowHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }

        [Theory]
        [InlineData(new int[] {0,3,6}, 'O', 1)]
        [InlineData(new int[] {0,3,6}, 'X', 2)]
        public void GivenFirstColumnHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }

        [Theory]
        [InlineData(new int[] {1,4,7}, 'O', 1)]
        [InlineData(new int[] {1,4,7}, 'X', 2)]
        public void GivenSecondColumnHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }

        [Theory]
        [InlineData(new int[] {2,5,8}, 'O', 1)]
        [InlineData(new int[] {2,5,8}, 'X', 2)]
        public void GivenThirdColumnHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }

        [Theory]
        [InlineData(new int[] {0,4,8}, 'O', 1)]
        [InlineData(new int[] {0,4,8}, 'X', 2)]
        public void GivenRightDiagonalHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }

        [Theory]
        [InlineData(new int[] {2,4,6}, 'O', 1)]
        [InlineData(new int[] {2,4,6}, 'X', 2)]
        public void GivenLeftDiagonalHasSameValues_ReturnsWinningPlayerNumber(
            int[] moveIndexes,
            char moveValue,
            int expected
        )
        {
            // Arrange
            var sut = CreateTicTacToeService();

            // Act
            SetPlayerMoves(sut, moveIndexes, moveValue);
            var actual = sut.GetWinner();

            // Assert
            Assert.True(actual == expected);
        }
    }
}