namespace TicTacToe.App.Services;

public class TicTacToeService
{
    private readonly char[] _board = {'0','1','2','3','4','5','6','7','8'};

    public char[] Board { get => _board; }

    // Player 1 plays O
    // Playee 2 plays X
    public int GetWinner()
    {
        if (_board[0].Equals(_board[1]) && _board[1].Equals(_board[2]))
            return _board[0] == 'O' ? 1 : 2;
        if (_board[3].Equals(_board[4]) && _board[4].Equals(_board[5]))
            return _board[3] == 'O' ? 1 : 2;
        if (_board[6].Equals(_board[7]) && _board[7].Equals(_board[8]))
            return _board[6] == 'O' ? 1 : 2;
        if (_board[0].Equals(_board[3]) && _board[3].Equals(_board[6]))
            return _board[0] == 'O' ? 1 : 2;
        if (_board[1].Equals(_board[4]) && _board[4].Equals(_board[7]))
            return _board[1] == 'O' ? 1 : 2;
        if (_board[2].Equals(_board[5]) && _board[5].Equals(_board[8]))
            return _board[2] == 'O' ? 1 : 2;
        if (_board[0].Equals(_board[4]) && _board[4].Equals(_board[8]))
            return _board[0] == 'O' ? 1 : 2;
        if (_board[2].Equals(_board[4]) && _board[4].Equals(_board[6]))
            return _board[2] == 'O' ? 1 : 2;

        return 0;
    }

    public void PlayerMove(int moveIndex, char value)
    {
        if (!(value == 'O' || value == 'X'))
        {
            throw new ArgumentException("Invalid move value.");
        }

        if (moveIndex < 0 || moveIndex > 9)
        {
            throw new ArgumentException("Invalid move index.");
        }

        _board[moveIndex] = value;
    }
}