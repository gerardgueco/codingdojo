﻿using TicTacToe.App.Services;

namespace TicTacToe.App;

public class Program
{
    public static void Main()
    {
        var ticTacToeService = new TicTacToeService();

        // Player 1 plays O
        // Playee 2 plays X

        // ask player 1 for their move
        // while (true) loop here?
        // it will be true if all index in the board has been filled or there is a winner already
        // draw console UI of TicTacToe board with filled values (from Board property of the service);
        //var result = ticTacToeService.GetWinner(new char[]{'O','O','O','O','X','O','X','X','O','X'});
        var result = ticTacToeService.GetWinner();
        Console.WriteLine($"Winner is: Player {result}");
    }
}