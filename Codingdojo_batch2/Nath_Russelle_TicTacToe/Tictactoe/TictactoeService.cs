﻿namespace Tictactoe
{
    public class TictactoeService
    {
        int winner = 0;

        public int checkWinner(Board board)
        {
            checkRows(board);
            if (winner == 0)
                checkColumns(board);
            if (winner == 0)
                checkDiagonal(board);    

            return winner;
        }

        private void checkRows(Board board) {
            if (board.a1 != null && board.a1 == board.a2 && board.a2 == board.a3)
                winner = checkWinnerPlayer(board.a1);
            else if (board.b1 != null && board.b1 == board.b2 && board.b2 == board.b3)
                winner = checkWinnerPlayer(board.b1);
            else if (board.c1 != null && board.c1 == board.c2 && board.c2 == board.c3)
                winner = checkWinnerPlayer(board.c1);
        }

        private void checkColumns(Board board) {
            if (board.a1 != null && board.a1 == board.b1 && board.b1 == board.c1)
                winner = checkWinnerPlayer(board.a1);
            else if (board.a2 != null && board.a2 == board.b2 && board.b2 == board.c2)
                winner = checkWinnerPlayer(board.a2);
            else if (board.a3 != null && board.a3 == board.b3 && board.b3 == board.c3)
                winner = checkWinnerPlayer(board.a3);
        }

        private void checkDiagonal(Board board) {
            if (board.a1 != null && board.a1 == board.b2 && board.b2 == board.c3)
                winner = checkWinnerPlayer(board.a1);
            else if (board.a3 != null && board.a3 == board.b2 && board.b2 == board.c1)
                winner = checkWinnerPlayer(board.a3);
        }

        private int checkWinnerPlayer(string value) {
            return value == "O" ? 1 : 2;
        }
    }
}
