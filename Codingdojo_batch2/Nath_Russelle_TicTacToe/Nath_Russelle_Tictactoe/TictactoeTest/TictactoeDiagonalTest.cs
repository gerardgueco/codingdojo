using Tictactoe;

namespace TictactoeTest
{
    public class TictactoeDiagonalTest
    {
        [Fact]
        public void Player1Win_First_Diagonal()
        {
            var board = new Board{
                a1 = "O",
                b2 = "O",
                c3 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_First_Diagonal()
        {
            var board = new Board{
                a1 = "X",
                b2 = "X",
                c3 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }

        [Fact]
        public void Player1Win_Second_Diagonal()
        {
            var board = new Board{
                a3 = "O",
                b2 = "O",
                c1 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_Second_Diagonal()
        {
            var board = new Board{
                a3 = "X",
                b2 = "X",
                c1 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }
    }
}