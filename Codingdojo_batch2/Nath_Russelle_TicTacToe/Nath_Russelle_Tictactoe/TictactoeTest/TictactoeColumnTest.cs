using Tictactoe;

namespace TictactoeTest
{
    public class TictactoeColumnTest
    {
        [Fact]
        public void Player1Win_First_Column()
        {
            var board = new Board{
                a1 = "O",
                b1 = "O",
                c1 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_First_Column()
        {
            var board = new Board{
                a1 = "X",
                b1 = "X",
                c1 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }

        [Fact]
        public void Player1Win_Second_Column()
        {
            var board = new Board{
                a2 = "O",
                b2 = "O",
                c2 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_Second_Column()
        {
            var board = new Board{
                a2 = "X",
                b2 = "X",
                c2 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }

        [Fact]
        public void Player1Win_Third_Column()
        {
            var board = new Board{
                a3 = "O",
                b3 = "O",
                c3 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_Third_Column()
        {
            var board = new Board{
                a3 = "X",
                b3 = "X",
                c3 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }
    }
}