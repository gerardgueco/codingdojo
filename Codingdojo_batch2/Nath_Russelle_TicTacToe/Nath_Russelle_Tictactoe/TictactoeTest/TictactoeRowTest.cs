using Tictactoe;

namespace TictactoeTest
{
    public class TictactoeRowTest
    {
        [Fact]
        public void Player1Win_First_Row()
        {
            var board = new Board{
                a1 = "O",
                a2 = "O",
                a3 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_First_Row()
        {
            var board = new Board{
                a1 = "X",
                a2 = "X",
                a3 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }

        [Fact]
        public void Player1Win_Second_Row()
        {
            var board = new Board{
                b1 = "O",
                b2 = "O",
                b3 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_Second_Row()
        {
            var board = new Board{
                b1 = "X",
                b2 = "X",
                b3 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }

        [Fact]
        public void Player1Win_Third_Row()
        {
            var board = new Board{
                c1 = "O",
                c2 = "O",
                c3 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(1, winner);
        }

        [Fact]
        public void Player2Win_Third_Row()
        {
            var board = new Board{
                c1 = "X",
                c2 = "X",
                c3 = "X"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(2, winner);
        }
    }
}