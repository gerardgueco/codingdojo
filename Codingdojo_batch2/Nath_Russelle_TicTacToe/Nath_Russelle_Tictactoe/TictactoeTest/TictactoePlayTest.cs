using Tictactoe;

namespace TictactoeTest
{
    public class TictactoePlayTest
    {
        [Fact]
        public void Tie_All_Fields_Taken()
        {
            var board = new Board{
                a1 = "X",
                b1 = "O",
                c1 = "O",
                a2 = "O",
                b2 = "X",
                c2 = "X",
                a3 = "X",
                b3 = "O",
                c3 = "O"
            };

            TictactoeService tictactoeService = new TictactoeService();

            var winner = tictactoeService.checkWinner(board);
            Assert.Equal(0, winner);
        }
    }
}