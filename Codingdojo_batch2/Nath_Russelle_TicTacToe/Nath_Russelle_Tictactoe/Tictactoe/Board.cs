using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tictactoe
{
    public class Board
    {
        public string? a1 { get; set; }
        public string? a2 { get; set; }
        public string? a3 { get; set; }
        public string? b1 { get; set; }
        public string? b2 { get; set; }
        public string? b3 { get; set; }
        public string? c1 { get; set; }
        public string? c2 { get; set; }
        public string? c3 { get; set; }
    }
}