export interface ITicTacToe
{
  value: number;
  isSelected: boolean;
	playerId: number;
}